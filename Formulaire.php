<!Doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Mon Menu</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <div class="formulaire">
        <h1>Formulaire pour acheter un Lexus LC500 Essence ou Hybride</h1>

        <form method="post" action="Traitement_Formulaire.php" enctype="multipart/form-data">
            <div>
                <label for="nom"><strong>Votre nom :</strong></label>
                <input type="text" id="nom" name="nom" placeholder="Entrer votre nom" required>
            </div>

            <br />

            <div>
                <label for="prenom"><strong>Votre prénom :</strong></label>
                <input type="text" id="prenom" name="prenom" placeholder="Entrer votre prenom" required>
            </div>

            <br />

            <div>
                <label for="adresse"><strong>Votre adresse : </strong></label>
                <input type="text" id="adresse" name="adresse" placeholder="Entrer votre adresse" required>
            </div>

            <br />

            <div>
                <label for="ville"><strong>Votre ville : </strong></label>
                <input type="text" id="ville" name="ville" placeholder="Entrer votre ville" required>
            </div>

            <br />

            <div>
                <label for="code_postal"><strong>Votre code postal : </strong></label>
                <input type="number" id="code_postal" name="code_postal" placeholder="Entrer votre code postale" required>
            </div>

            <br />

            <div>
                <label for="email"><strong>Votre e-mail : </strong></label>
                <input type="email" id="email" name="email" placeholder="Entrer votre email" required>
            </div>

            <br />

            <div>
                <label for="telephone"><strong>Numeros de téléphone : </strong> </label>
                <input type="number" id="telephone" name="telephone" placeholder="Entrer votre telephone" required>
            </div>

            <br />

            <div>
                <button type="submit">Envoyer</button>
            </div>
        </form>

    </div>
</body>

</html>