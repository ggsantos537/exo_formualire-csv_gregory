<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>AFFICHAGE</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php

    include 'enregistrements.csv';

    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $adresse = $_POST['adresse'];
        $ville = $_POST['ville'];
        $code_postal = $_POST['code_postal'];
        $telephone = $_POST['telephone'];
        $email = $_POST['email'];


        $fichierExiste = file_exists("enregistrements.csv");


        $fichier = fopen("enregistrements.csv", "a");


        if (!$fichierExiste) {
            fwrite($fichier, "---------------------------\n");
        }

        $dateHeure = date("Y-m-d H:i:s");
        fwrite($fichier, "Date et heure : " . $dateHeure . "\n");
        fwrite($fichier, "Nom : " . $nom . "\n");
        fwrite($fichier, "Prenom : " . $prenom . "\n");
        fwrite($fichier, "Adresse : " . $adresse . "\n");
        fwrite($fichier, "Ville : " . $ville . "\n");
        fwrite($fichier, "Code Postal : " . $code_postal . "\n");
        fwrite($fichier, "Telephone : " . $telephone . "\n");
        fwrite($fichier, "Email : " . $email . "\n");
        fwrite($fichier, "---------------------------\n");
        fclose($fichier);

        echo "<h1>Donnees enregistrees :</h1>";
        echo "<table>";
        echo "<tr><th>Date et heure</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Ville</th><th>Code Postal</th><th>Telephone</th><th>Email</th></tr>";
        echo "<tr><td>$dateHeure</td><td>$nom</td><td>$prenom</td><td>$adresse</td><td>$ville</td><td>$code_postal</td><td>$telephone</td><td>$email</td></tr>";
        echo "</table>";


        echo "<h1>Données du fichier enregistrements.csv :</h1>";

        $contenu = file_get_contents("enregistrements.csv");


        $enregistrements = explode("---------------------------\n", $contenu);


        $tablecsv = "<table>\n";
        $tablecsv .= "<tr><th>Date et heure</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Ville</th><th>Code Postal</th><th>Telephone</th><th>Email</th></tr>\n";


        foreach ($enregistrements as $enregistrement) {

            $enregistrement = trim($enregistrement);


            if (!empty($enregistrement)) {

                $champs = explode("\n", $enregistrement);


                $dateHeure = substr($champs[0], 15);
                $nom = substr($champs[1], 6);
                $prenom = substr($champs[2], 9);
                $adresse = substr($champs[3], 10);
                $ville = substr($champs[4], 7);
                $codePostal = substr($champs[5], 14);
                $telephone = substr($champs[6], 12);
                $email = substr($champs[7], 8);


                if (empty($dateHeure) || empty($nom) || empty($prenom) || empty($adresse) || empty($ville) || empty($codePostal) || empty($telephone) || empty($email)) {

                    if (empty($dateHeure)) {
                        $dateHeure = "N/A";
                    }
                    if (empty($nom)) {
                        $nom = "N/A";
                    }
                    if (empty($prenom)) {
                        $prenom = "N/A";
                    }
                    if (empty($adresse)) {
                        $adresse = "N/A";
                    }
                    if (empty($ville)) {
                        $ville = "N/A";
                    }
                    if (empty($codePostal)) {
                        $codePostal = "N/A";
                    }
                    if (empty($telephone)) {
                        $telephone = "N/A";
                    }
                    if (empty($email)) {
                        $email = "N/A";
                    }
                }


                $tablecsv .= "<tr><td>$dateHeure</td><td>$nom</td><td>$prenom</td><td>$adresse</td><td>$ville</td><td>$codePostal</td><td>$telephone</td><td>$email</td></tr>\n";
            }
        }


        $tablecsv .= "</table>\n";


        echo $tablecsv;
    }
    ?>
</body>

</html>
